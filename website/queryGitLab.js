const fs = require("fs");
const path = require("path");
const axios = require("axios");

const sidebars = JSON.parse(fs.readFileSync("sidebars.json"));
const GITLAB_DOC_FOLDER = "_gitlab";

const gitlabClient = axios.create({
  method: "get",
  headers: { "Private-Token": process.env.GITLAB_TOKEN }
});

function touchDir(dir) {
  // synchronous on purpose
  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir);
  }
}

async function nestedApply(obj, fn) {
  const res = await (Array.isArray(obj) ? obj : Object.values(obj)).reduce(
    async (arr, value) =>
      typeof value === "string"
        ? await fn(value)
        : await nestedApply(value, fn),
    []
  );
  return res;
}

async function handleDoc(docId) {
  if (!docId.startsWith(GITLAB_DOC_FOLDER)) {
    return;
  }
  const { projectId, filePath, ref } = parseDocId(docId);

  await fetchRawContent(
    projectId,
    filePath,
    ref,
    async ({ data: rawContent }) => {
      const content = `${generateDocHeader(
        projectId,
        filePath,
        ref
      )}${rawContent}`;
      const docPath = `../docs/${GITLAB_DOC_FOLDER}/${projectId}/${filePath}`;
      touchDir(path.dirname(docPath));
      await fs.writeFile(docPath, content);
    }
  );
}

async function fetchRawContent(projectId, filePath, ref, saveFile) {
  const url = `/projects/${projectId}/repository/files/${encodeURIComponent(
    filePath
  )}/raw?ref=${ref}`;
  console.log("Fetching: ", url);
  await gitlabClient(`https://gitlab.com/api/v4${url}`)
    .then(saveFile)
    .catch(() =>
      console.error(
        `Failed to fetch doc: ${formatDocId(projectId, filePath, ref)}`
      )
    );
}

function formatDocId(projectId, filePath, ref) {
  return `${GITLAB_DOC_FOLDER}/${projectId}#${ref}/${filePath.slice(
    0,
    filePath.length - ".md".length
  )}`;
}

function parseDocId(docId) {
  const [, projectRef, ...filePaths] = docId.split("/");
  const [projectId, ref = "master"] = projectRef.split("#");
  return {
    projectId,
    ref,
    filePath: filePaths.length ? filePaths.join("/") + ".md" : "README.md"
  };
}

function formatDocTitle(filePath) {
  return path.basename(filePath).split(".")[0];
}

function generateDocHeader(projectId, filePath, ref) {
  return `---
title: ${formatDocTitle(filePath)}
---

`;
}

nestedApply(sidebars, handleDoc);
